<?php declare(strict_types=1);

namespace SpamAssasin\View;

class Page
{
    private $template;
    private $title = 'Untitled page';

    public function __construct(string $template = 'blank')
    {
        $templatePath = __DIR__ . '/templates/' . $template . '.phtml';
        if (!is_readable($templatePath)) {
            throw new \RuntimeException('Nie udało się odnazleźć pliku z szablonem');
        }
        $this->template = $templatePath;
    }

    public function render(array $params): string
    {
        extract($params);
        $template = $this->template;
        ob_start();
        include_once __DIR__ . '/templates/index.phtml';
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }
    
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }    
}