<?php declare(strict_types=1);

namespace SpamAssasin\Data;

use SpamAssasin\Exception\SpamListException;
                
class Config 
{
    private $configFilePath;
    private $configData;
    private $isConfigFileLoaded = false;
        
    public function __construct(string $path)
    {
        $this->configFilePath = $path;
        $this->loadConfigFile();
    }
    
    public function getRoutes(): array
    {
        return $this->configData['Routes'];
    }
    
    public function getDatabaseFilePath(): string
    {
        return $this->configData['DataBaseFilePath'];
    }
    
    public function getHomeURL(): string
    {
        return $this->configData['HomeURL'];
    }
    
    public function getBlacklistPrefix(): string
    {
        return $this->configData['BlacklistItemPrefix'];
    }
    
    public function getBlacklistSuffix(): string
    {
        return $this->configData['BlacklistItemSuffix'];
    }
    
    private function loadConfigFile(): void
    {
        if (!$this->isConfigFileLoaded) {
            if (is_readable($this->configFilePath)) {
                $this->configData = json_decode(file_get_contents($this->configFilePath), true);   
                $this->isConfigFileLoaded = true;
            } else {
                throw SpamListException::forReadError($this->configFilePath);
            }
        }
    }
}