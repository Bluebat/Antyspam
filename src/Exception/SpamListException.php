<?php declare(strict_types=1);

namespace SpamAssasin\Exception;

class SpamListException extends \RuntimeException
{
    public static function forWriteError(string $filePath): SpamListException
    {
         return new self("Could not perisis entry, cannot write to file ${filePath}");
    }
    
    public static function forReadError(string $filePath): SpamListException
    {
         return new self("Cannot open file ${filePath}");
    }
}