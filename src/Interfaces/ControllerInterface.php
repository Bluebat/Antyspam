<?php declare(strict_types=1);

namespace SpamAssasin\Interfaces;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

interface ControllerInterface
{
    public function __invoke(Request $request, Response $response);
}
