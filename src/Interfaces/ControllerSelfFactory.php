<?php declare(strict_types=1);

namespace SpamAssasin\Interfaces;

use Psr\Container\ContainerInterface;
use SpamAssasin\Data\Config;

interface ControllerSelfFactory
{
    public function __construct(Config $config);
    public static function create(ContainerInterface $container): ControllerInterface;
}
