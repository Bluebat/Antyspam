<?php declare(strict_types=1);

namespace SpamAssasin\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use SpamAssasin\Interfaces\ControllerInterface;
use SpamAssasin\Interfaces\ControllerSelfFactory;
use SpamAssasin\Data\Config;
use SpamAssasin\View\Page;
use SpamAssasin\Model\SpamList;


final class SaveController implements ControllerInterface, ControllerSelfFactory
{    
    private $config = [];
    
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $domainToInsertOnBlacklist = $request->getParsedBody()['addDomainToBlackList'];
        $spammList = new SpamList($this->config->getDatabaseFilePath());  
        if ($spammList->addItem($this->config, $domainToInsertOnBlacklist)){
            $msg = 'success.phtml';
        } else{
            $msg = 'failure.phtml';
        }                
        $webPage = new Page('form');
        $webPage->setTitle('Zapis nowego rekordu');
        $response->getBody()->write($webPage->render(['item' => $domainToInsertOnBlacklist, 'duplicatedItems' => $spammList->getDuplicatedItems(), 'msg' => $msg]));
        
        return $response;
    }
    
    /**
     * Static method factory pattern
     *
     * @param ContainerInterface $container
     * @return ControllerInterface
     */
    public static function create(ContainerInterface $container): ControllerInterface
    {
        return new self($container->get('config'));
    }
}