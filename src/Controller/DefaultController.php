<?php declare(strict_types=1);

namespace SpamAssasin\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use SpamAssasin\Interfaces\ControllerInterface;
use SpamAssasin\Interfaces\ControllerSelfFactory;
use SpamAssasin\Data\Config;


final class DefaultController implements ControllerInterface, ControllerSelfFactory
{    
    private $config = [];
    
    public function __construct(Config $config)
    {
        $this->config = $config;
    }
    
    public function __invoke(Request $request, Response $response): Response
    {
        
        return $response
            ->withStatus(302)
            ->withHeader('Location', "{$request->getUri()->getScheme()}://{$request->getUri()->getHost()}:{$request->getUri()->getPort()}{$this->config->getHomeURL()}");
    }
    
    /**
     * Static method factory pattern
     *
     * @param ContainerInterface $container
     * @return ControllerInterface
     */    
    public static function create(ContainerInterface $container): ControllerInterface
    {
        return (new self($container->get('config')));
    }
}