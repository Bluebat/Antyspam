<?php declare(strict_types=1);

namespace SpamAssasin\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use SpamAssasin\Interfaces\ControllerInterface;
use SpamAssasin\View\Page;

final class IndexController implements ControllerInterface
{
    public function __invoke(Request $request, Response $response): Response
    {  
        $webPage = new Page('form');
        $webPage->setTitle('Formularz dodania nowej spamerskiej domeny');
        $response->getBody()->write($webPage->render([]));
        
        return $response;
    }
}