<?php declare(strict_types=1);

namespace SpamAssasin\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use SpamAssasin\Interfaces\ControllerInterface;
use SpamAssasin\Interfaces\ControllerSelfFactory;
use SpamAssasin\View\Page;
use SpamAssasin\Model\SpamList;
use SpamAssasin\Data\Config;

class ListController implements ControllerInterface, ControllerSelfFactory
{    
    private $config = [];
    
    public function __construct(Config $config)
    {
        $this->config = $config;
    }
    
    public function __invoke(Request $request, Response $response): Response
    {       
        $spammList = new SpamList($this->config->getDatabaseFilePath());   
        $webPage = new Page('displayBlacklistEntries');
        $webPage->setTitle('Konfiguracja SpammAssasin`a');
        $response
            ->getBody()
            ->write(
                $webPage->render(
                    ['items' => $spammList->showData()]
                )
            );
        
        return $response;
    }
    
    /**
     * Static method factory pattern
     *
     * @param ContainerInterface $container
     * @return ControllerInterface
     */    
    public static function create(ContainerInterface $container): ControllerInterface
    {
        return (new self($container->get('config')));
    }
}