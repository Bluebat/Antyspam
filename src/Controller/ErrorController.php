<?php declare(strict_types=1);

namespace SpamAssasin\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use SpamAssasin\Interfaces\ControllerInterface;

class ErrorController implements ControllerInterface
{    
    public function __invoke(Request $request, Response $response): Response
    {
        
        return $response->withStatus(500)->withHeader('Content-Type', 'text/html');            
    }
    
    public function error404(Request $request, Response $response): Response
    {
        
        return $response
                ->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write("Page <strong>{$request->getUri()->getScheme()}://{$request->getUri()->getHost()}:{$request->getUri()->getPort()}{$request->getUri()->getPath()}</strong> not found");         
    }    
}