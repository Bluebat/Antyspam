<?php declare(strict_types=1);

namespace SpamAssasin\Model;

use SpamAssasin\Data\Config;
use SpamAssasin\Exception\SpamListException;

final class SpamList
{
    private $items = [];
    private $filepath = '';
    private $isDataLoaded = false;
    private $config;
    private $duplicatedItem = '';
    
    public function __construct(string $datapath)
    {
        $this->filepath = $datapath;
        $this->loadContent();
    }

    public function showData(): array
    {
        return $this->items;
    }
       
    private function loadContent(): void
    {        
        if ($this->isDataLoaded === false)
        {
            $this->items = file($this->filepath, FILE_IGNORE_NEW_LINES);
            $this->isDataLoaded = true;              
        }
    }
    
    public function getDuplicatedItems(): string
    {
        return $this->duplicatedItem;
    }
    
    private function isValidItem(string $item): bool
    {
        if (substr_count($item, ';') > 0) {
            return true;
        }
        return false;
    }

    private function isMultiRecordItem(string $item): bool
    {
        if (substr_count($item, ';') > 1) {
            return true;
        }
        return false;
    }
    
    private function addMultipleItems(string $items): bool
    {
        $records = array_filter(explode(';', $items)); 
        foreach ($records as $record){
            if (!$this->saveItem($record)){
                $this->duplicatedItem = $record ;
                return false;
            }
        }
        return true;
    }

    public function addItem(Config $config, string $item): bool
    {
        $this->config = $config;
        if (!$this->isValidItem($item)) {
            return false;
        } elseif ($this->isMultiRecordItem($item)) {
            if (!$this->addMultipleItems($item)) {           
                return false;
            }
        } elseif (!$this->saveItem($item)) {
            $this->duplicatedItem = $item;
            return false;
        }   
        return true;
    }
    
    private function clearItemBeforeSave(string $item): string
    {
        return trim(str_replace(';', '', $item));
    }
	
    private function saveItem(string $raw_item): bool
    {
        $item = $this->clearItemBeforeSave($raw_item);
        if (empty($item)){
            return false;
        } elseif ($this->hasItem($item)) {
            $this->duplicatedItem = $item;
            return false;
        }
        if (file_put_contents($this->filepath, PHP_EOL . $this->addPrefixAndSuffix($item), FILE_APPEND | LOCK_EX)){
            return true;
        }
        throw SpamListException::forWriteError($this->filepath);
    }


    private function addPrefixAndSuffix($item): string
    {
        if (!$this->config){
            return $item;
        }
        return $this->config->getBlacklistPrefix() . $item . $this->config->getBlacklistSuffix();
    }

    public function hasItem(string $item): bool
    {
        $this->loadContent();
        return in_array( $this->addPrefixAndSuffix($item) , $this->items);
    }
}