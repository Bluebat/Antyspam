<?php declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Slim\App as HttpRouter;
use Slim\Container as Container;
use Slim\Http\Response as HttpResponse;
use Slim\Http\Request as HttpRequest;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use SpamAssasin\Data\Config;
use SpamAssasin\Controller\ErrorController;
use SpamAssasin\Interfaces\ControllerSelfFactory;

(new class() {
	
    private $router;
    private $config;
    private $container;

    public function __construct()
    {
        $this->getConfig();
        $this->router = new HttpRouter($this->runRouterConfig());
    }
    
    private function runRouterConfig(): Container
    {
        if (!$this->config) {
            throwException();
        }
        $container = new Container(); 
        $container['config'] = $this->config;
        $container['notFoundHandler'] = function($container) {
            return function(Request $request, Response $response) {
                return (new ErrorController())->error404($request, $response);
            };
        };
        
        return $this->container = $container;
    }
    
    private function getConfig(): void
    {
        $configPath = 'src/Data/Config.json';
        try {
            $this->config = new Config($configPath);            
        } catch  (Exception $e) { 
            error_log ("Fatal error. Can not open config file $configPath."); 
            exit(); //remove error
        }                   
    }

    public function setRouting(): void
    {
        foreach ($this->config->getRoutes() as $route => $dispatch)
        {
            $container = $this->container;
            $this->router->{$dispatch['method']} ($route, function(Request $request, Response $response) use ($dispatch, $container){       
                $reflect = new ReflectionClass($dispatch['controller']); //@todo better way of checking haveInterface               
                if ($reflect->implementsInterface('SpamAssasin\Interfaces\ControllerSelfFactory')) {
                    
                    return ($dispatch['controller']::create($container))($request, $response); 
                } else {
                    
                    return (new $dispatch['controller']())($request, $response);          
                }
            });
        }
    }

    public function __invoke(): void
    {
        $this->setRouting();
        $this->router->run();
    }
})();