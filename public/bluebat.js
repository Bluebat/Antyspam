function onlyUnique(value, index, self) {
       return self.indexOf(value) === index;
}

function showItem(item, index) {
    $('#response').append('<div class="mailAddress button alert">'+item+'</div> &nbsp;  &nbsp;  &nbsp; ');
}

function extractEmails (i)
{
    var str = $('#rawData').val();
    str = str.replace(/bluebat.pl/g, '');
    var match = str.match(/(@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    match = match.filter( onlyUnique );
    match.forEach(showItem);
    $('.mailAddress').click(function(){
        $('#addDomainToBlackList').val($('#addDomainToBlackList').val()+$(this).text()+';');
    });
    return i++;
}

$(function(){
    $(document).foundation();
    $('#rawData').keyup(function( event ) {
        if (event.which !== 86 ){event.preventDefault(); return false;}
        extractEmails();
    });
});

